<?php

namespace App\models;
use ActiveRecord\Model;

class Professor extends Model
{
	static $table_name = "tb_professores";

	public function listarProfessor()
	{
		return parent::find('all');
		//return "teste";
	}

	public function login($email,$senha)
	{
		return parent::find('all',array('conditions' => array('email = ? AND senha = ?', $email, $senha)));
	}

}