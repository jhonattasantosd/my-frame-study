<?php

namespace App\models;
use ActiveRecord\Model;

class Usuario extends Model
{
	static $table_name = "tb_usuarios";

	public function listarUsuarios()
	{
		return parent::find('all');		
	}

	public function login($email,$senha)
	{
		$email = filter_var($email, FILTER_VALIDATE_EMAIL);
		$senha = filter_var($senha, FILTER_SANITIZE_STRING);

		if($email){				
			$email = filter_var($email, FILTER_SANITIZE_EMAIL);	
			try{
				$usuario = parent::find(array('conditions' => array('email = ? AND senha = ?', $email, $senha)));
				session_start();
				$_SESSION['usuario_logado'] = true;	
				$_SESSION['usuario_nome'] = $usuario->nome;		
				$tipoUsuario = $usuario->tipo;
				switch ($tipoUsuario) {
					case 'A':
						header('Location: panel/aluno');				
						break;			
					case 'P':
						header('Location: panel/professor');
						break;
				}	
				
			}catch(\Exception $e){
				echo "Email ou Senha inválida";
			}
					
		}else{
			#retorna para pagina inicial com a mensagem de e-mail invalido
			echo "Email invalido";
		}
		
	}

}