<?php

require __DIR__.'/../vendor/autoload.php';

use ActiveRecord\Config;

$whoops = new \Whoops\Run;
$whoops->pushHandler(new \Whoops\Handler\PrettyPageHandler);
$whoops->register();

$cfg = Config::instance();
$cfg->set_model_directory('app/models');
$cfg->set_connections(array(
         'development' => 'mysql://root:root@localhost/jquery'));
