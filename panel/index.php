<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Login Sistema Escolar</title>
	<link rel="stylesheet" href="public/bower_components/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="public/bower_components/bootstrap/dist/css/bootstrap.min.css">
</head>
<body>

	<div class="container">
		<div class="row">
			<div class="col-md-4 col-md-offset-4">
				<h1 class="text-center">Login</h1>
				<form action="../login.php" method="post">
				  <div class="form-group">
				    <label for="email">Email</label>
				    <input type="text" class="form-control" id="email" placeholder="Email" name="email">
				  </div>
				  <div class="form-group">
				    <label for="senha">Senha</label>
				    <input type="password" class="form-control" id="senha" placeholder="Senha" name="senha">
				  </div>	  
				  <div class="checkbox">
				    <label>
				      <input type="checkbox"> Lembrar
				    </label>
				  </div>
				  <button type="submit" class="btn btn-primary pull-left">Login</button>
				</form>
			</div>
		</div>
	</div>

	<script type="text/javascript" src="public/bower_components/jquery/dist/jquery.min.js"></script>
	<script type="text/javascript" src="public/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="public/js/login.js"></script>
	
</body>
</html>